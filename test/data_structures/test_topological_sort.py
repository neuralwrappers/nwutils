from nwutils.data_structures import topological_sort

class Test_topological_sort:
    def test_topological_sort_1(self):
        dep_graph = {
            "A":[],
            "B":["A"],
            "C":[],
            "D":["A", "C"]
        }
        topoSortedGraph = topological_sort(dep_graph)
        assert len(topoSortedGraph) == len(dep_graph.keys())
        assert topoSortedGraph.index("B") > topoSortedGraph.index("A")
        assert topoSortedGraph.index("D") > topoSortedGraph.index("A")
        assert topoSortedGraph.index("D") > topoSortedGraph.index("C")

    def test_topological_sort_2(self):
        dep_graph = {
            "A":["D"],
            "B":["A"],
            "C":["B"],
            "D":["C"],
        }
        try:
            topoSortedGraph = topological_sort(dep_graph)
            assert False
        except:
            pass

    def test_topological_sort_3(self):
        dep_graph = {
            "A":[],
            "B":["A"],
            "C":["Y"],
        }
        try:
            topoSortedGraph = topological_sort(dep_graph)
            assert False
        except:
            pass
