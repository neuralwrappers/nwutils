import numpy as np
from nwutils.data import running_mean

class TestRunningMean:
	def test_RunningMean_update_1(self):
		rm = running_mean(0)
		for i in range(10):
			rm.update(i)
		assert abs(rm.get() - 4.5) < 1e-5

	def test_RunningMean_update_2(self):
		rm = running_mean([0, 0])
		for i in range(10):
			rm.update([i, i + 1])
		assert np.abs(rm.get() - [4.5, 5.5]).sum() < 1e-5

	def test_RunningMean_update_3(self):
		rm = running_mean({"a" : 0, "b" : 0})
		for i in range(10):
			rm.update({"b" : i, "a" : i + 1})
		assert np.abs(np.array(list(rm.get().values())) - [5.5, 4.5]).sum() < 1e-5

	def test_RunningMean_update_batch_1(self):
		rm = running_mean(0)
		rm.update_batch([1,2,3])
		assert abs(rm.get() - 2) < 1e-5

	def test_RunningMean_update_batch_2(self):
		rm = running_mean([0, 0])
		rm.update_batch(np.arange(8).reshape((4, 2)))
		assert np.abs(rm.get() - [3, 4]).sum() < 1e-4

if __name__ == "__main__":
	TestRunningMean().test_RunningMean_update_1()