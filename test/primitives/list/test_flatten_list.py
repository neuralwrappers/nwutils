from nwutils.primitives.list import flatten_list

class TestFlattenList:
    def test_flatten_list_1(self):
        a = []
        assert flatten_list(a) == []

    def test_flatten_list_2(self):
        a = [1, 2, 3, 4, 5]
        assert flatten_list(a) == [1, 2, 3, 4, 5]

    def test_flatten_list_3(self):
        a = [1, [2, 3], [4, [5, [6]]]]
        res = flatten_list(a)
        assert res == [1, 2, 3, 4, 5, 6]

    def test_flatten_list_4(self):
        a = [{1, 2}, [{"1": 2, "2": {"2_1": 2, "2_2": 3}}, (1, 2, 3)], ["hi", "there"], "hi", None]
        assert flatten_list(a) == [{1, 2}, {"1": 2, "2": {"2_1": 2, "2_2": 3}}, (1, 2, 3), "hi", "there", "hi", None]
    
    def test_flatten_list_5(self):
        a = {"a": [1, 2, 3], "b": [5, 6, (7, )]}
        assert flatten_list(list(a.values())) == [1, 2, 3, 5, 6, (7, )]

if __name__ == "__main__":
    TestFlattenList().test_flatten_list_3()
