from nwutils.primitives.dict import dict_deep_get

class Test_dict_deep_get:
    def test_dict_deep_get_1(self):
        D = {"1":1, "2":2}
        assert dict_deep_get(D, "1") == 1
    
    def test_dict_deep_get_2(self):
        D = {"1":1, "2":2}
        try:
            _ = dict_deep_get(D, "3")
        except KeyError:
            pass

    def test_dict_deep_get_3(self):
        D = {"1":{"2":3, "3":4}, "2":{"99":100}}
        assert dict_deep_get(D, ["1", "2"]) == 3
        assert dict_deep_get(D, ["2", "99"]) == 100

    def test_dict_deep_get_4(self):
        D = {"1":{"2":3, "3":4}, "2":{"99":100}}
        try:
            _ = dict_deep_get(D, ["1", "3"])
        except KeyError:
            pass

    def test_dict_deep_get_5(self):
        D = {"1":{"2":3, "3":4}, "2":{"99":100}}
        assert dict_deep_get(D, ("1", "3")) == 4
        assert dict_deep_get(D, "2") == {"99":100}
