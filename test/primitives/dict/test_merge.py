from nwutils.primitives.dict import dict_merge

class Test_dict_merge:
    def test_dict_merge_1(self):
        a = {"a":10}
        b = {"b":5}
        expected = {"b":5, "a":10}
        c = dict_merge(a, b)
        assert c == expected

    def test_dict_merge_2(self):
        a = {"a":{"c":10}}
        b = {"b":{"c":5}}
        expected = {"b":{"c":5}, "a":{"c":10}}
        expected2 = {"a":{"c":10}, "b":{"c":5}}
        c = dict_merge(a, b)
        assert c == expected and c == expected2
    
    def test_dict_merge_3(self):
        a={"a":5}
        b={"a":10}
        try:
            c = dict_merge(a, b)
            assert False
        except AssertionError:
            pass

    def test_dict_merge_4(self):
        a={"a":5, "b":100}
        b={"c":50, "a":[1,2,3]}
        try:
            c = dict_merge(a, b)
            assert False
        except AssertionError:
            pass

    def test_dict_merge_5(self):
        a = {"a":{"c":10}}
        b = {"b":{"c":5}}
        expected = {"b":{"c":5}, "a":{"c":10}}
        expected2 = {"a":{"c":10}, "b":{"c":5}}
        c = dict_merge(a, b, inplace=True)
        assert id(a) == id(c)
        assert c == expected and c == expected2

    def test_dict_merge_6(self):
        a = {"a":{"c":10}}
        b = {"b":{"c":5}}
        expected_a = {"a":{"c":10}}
        expected = {"b":{"c":5}, "a":{"c":10}}
        expected2 = {"a":{"c":10}, "b":{"c":5}}
        c = dict_merge(a, b, inplace=False)
        assert id(a) != id(c)
        assert a == expected_a
        assert c == expected and c == expected2

if __name__ == "__main__":
    Test_dict_merge().test_dict_merge_5()
