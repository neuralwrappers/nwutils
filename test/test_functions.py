from nwutils.functions import map_list

def test_map_list_1():
    items = [1, 2, 3]
    f1 = lambda x: x + 1
    f2 = lambda x: x * 2

    res = list(map_list([f1, f2], items))
    assert res == [4, 6, 8]

    res = list(map_list([f2, f1], items))
    assert res == [3, 5, 7]
