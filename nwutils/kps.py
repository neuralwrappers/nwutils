"""Keypoints utility functions"""
from typing import Tuple
import numpy as np
import cv2
from .color import ColorType, Blue

def rescale_kps(kps: np.ndarray, current_scale: Tuple[int, int], new_scale: Tuple[int, int]) -> np.ndarray:
    """Rescales a set of keypoints. Usually used when resizing an image to synchronize they attached keypoints"""
    assert len(current_scale) == 2, current_scale
    assert len(new_scale) == 2, new_scale

    h_rapp = new_scale[0] / current_scale[0]
    w_rapp = new_scale[1] / current_scale[1]
    # For safety, convert keypoints to float, apply the scaling factor, then convert back to integers
    kps = np.float32(kps) * [h_rapp, w_rapp]
    kps = np.int32(kps)
    return kps

def frame_keypointer(image: np.ndarray, kps: np.ndarray, radius_percent: float = 1,
                     color: ColorType = None) -> np.ndarray:
    """Given an image and a set of keypoints, draw circles of a particular radius and color on top of the image"""
    assert len(kps.shape) == 2 and kps.shape[1] == 2
    assert image.dtype == np.uint8
    if color is None:
        color = Blue
    h, w = image.shape[0], image.shape[1]
    # Copy the image so we create a new one instead of modifying the original ones
    image = image.copy()
    # Radius is taken as the minimum of the width/height times the percent (so they're circles). If below 1, then 1.
    radius = max(1, int(min(h, w) * radius_percent / 100))
    thickness = -1

    for kp_h, kp_w in kps:
        assert 0 <= kp_h < h, f"{kp_h} vs {h}"
        assert 0 <= kp_w < w, f"{kp_w} vs {h}"
        cv2.circle(image, (kp_h, kp_w), radius, color, thickness) #pylint: disable=no-member
    return np.array(image)
