"""Torch related utility functions"""
from typing import T, Callable, Union, Dict
import torch as tr
import numpy as np
from torch import optim, nn

from .numpy import np_get_data
from .logger import logger


def tr_get_data(data):
    logger.warning("use to_tensor")
    return to_tensor(data)


def tr_to_device(data, device):
    logger.warning("use to_device")
    return to_device(data, device)


# pylint: disable=too-many-return-statements
def to_tensor(data):
    """Cast data to torch tensor. There is no need for device as `lightning` handles this by itself."""
    if data is None:
        return None
    if isinstance(data, (np.int32, np.int8, np.int16, np.int64, np.float32, np.float64, int, float)):
        return tr.Tensor([data])
    if isinstance(data, list):
        return [to_tensor(x) for x in data]
    if isinstance(data, tuple):
        return tuple(to_tensor(x) for x in data)
    if isinstance(data, set):
        return {to_tensor(x) for x in data}
    if isinstance(data, dict):
        return {k: to_tensor(data[k]) for k in data}
    if isinstance(data, tr.Tensor):
        return data
    if isinstance(data, np.ndarray):
        if data.dtype == object or np.issubdtype(data.dtype, np.unicode_):
            return to_tensor(data.tolist())
        return tr.from_numpy(data)
    if callable(data):
        return data
    if isinstance(data, str):
        return data
    raise TypeError(f"Got unknown type: {type(data)}")


def to_device(data, device: tr.device):
    """Moves a generic parameter to the desired torch device."""
    if isinstance(data, (tr.Tensor, nn.Module)):
        return data.to(device)
    if isinstance(data, list):
        return [to_device(x, device) for x in data]
    if isinstance(data, tuple):
        return tuple(to_device(x, device) for x in data)
    if isinstance(data, set):
        return {to_device(x, device) for x in data}
    if isinstance(data, dict):
        return {k: to_device(data[k], device) for k in data}
    if isinstance(data, dict):
        return dict({k: to_device(data[k], device) for k in data})
    if isinstance(data, np.ndarray):
        if data.dtype == object or np.issubdtype(data.dtype, np.unicode_):
            return to_device(data.tolist(), device)
        return tr.from_numpy(data).to(device)  # pylint: disable=no-member
    if isinstance(data, (int, float, bool, str)):
        return data
    raise TypeError(f"Got unknown type: {type(data)}")


def tr_detach_data(data: T) -> T:
    """Calls detach on compounded torch data"""
    if data is None:
        return None

    if isinstance(data, tr.Tensor):
        return data.detach()

    if isinstance(data, list):
        return [tr_detach_data(x) for x in data]

    if isinstance(data, tuple):
        return tuple(tr_detach_data(x) for x in data)

    if isinstance(data, set):
        return {tr_detach_data(x) for x in data}

    if isinstance(data, dict):
        return {k: tr_detach_data(data[k]) for k in data}

    logger.debug2(f"Got unknown type {type(data)}. Returning as is.")
    return data


def np_to_tr_call(fn: Callable, *args, **kwargs):
    """Calls a torch function using numpy data, by first converting them to torch data"""
    return np_get_data(fn(*to_tensor(args), **to_tensor(kwargs)))


def tr_to_np_call(fn: Callable, *args, **kwargs):
    """Calls a numpy function using torch data, by first converting them to numpy data"""
    return to_tensor(fn(*np_get_data(args), **np_get_data(kwargs)))


def tr_get_optimizer_str(optimizer: Union[optim.Optimizer, Dict, None]) -> str:
    """Gets a string for a given optimizer of the torch library"""
    if isinstance(optimizer, dict):
        return ["Dict"]

    if optimizer is None:
        return ["None"]

    optimizer_str = None
    if isinstance(optimizer, tr.optim.SGD):
        groups = optimizer.param_groups[0]
        optimizer_str = "SGD"
        keys = ["lr", "momentum", "dampening", "weight_decay", "nesterov"]
        params_str = ", ".join([f"{k}: {groups[k]}" for k in keys])

    if isinstance(optimizer, (tr.optim.Adam, tr.optim.AdamW)):
        groups = optimizer.param_groups[0]
        keys = ["lr", "betas", "eps", "weight_decay"]
        optimizer_str = {tr.optim.Adam: "Adam", tr.optim.AdamW: "AdamW"}[type(optimizer)]
        params_str = ", ".join([f"{k}: {groups[k]}" for k in keys])

    if isinstance(optimizer, tr.optim.RMSprop):
        groups = optimizer.param_groups[0]
        optimizer_str = "RMSprop"
        keys = ["lr", "momentum", "alpha", "eps", "weight_decay"]

    assert optimizer_str is not None, f"Unknown optimizer: {optimizer}"
    params_str = ", ".join([f"{k}: {groups[k]}" for k in keys])
    f_str = f"{optimizer_str}. {params_str}"
    return f_str
