"""Init file"""
from .rgb_uint8_to_float import rgb_uint8_to_float, rgb_float_to_uint8, rgb_uint8_to_flat_uint24, \
    rgb_flat_uint24_to_uint8
