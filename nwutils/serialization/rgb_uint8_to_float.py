"""RGB uint8 to float representation module"""
import numpy as np

def rgb_uint8_to_float(x: np.ndarray) -> np.ndarray:
    """RGB uint8 to float function"""
    assert x.shape[-1] == 3, f"Expcted 3 channels, got: {x.shape}"
    assert x.dtype == np.uint8

    # Convert 3-channeled RGB uint8 to 1-channeled uint24
    flattened_x = rgb_uint8_to_flat_uint24(x)
    float_x = flattened_x.astype(np.float32)
    # Reduce to [0 : 1]
    float_x /= (256 * 256 * 256 - 1)
    # Convert to float32 for safety
    y = float_x.astype(np.float32)
    return y

# @brief Converts a float32 value to 24 bit RGB similar to the ones exported by Unreal Engine
def rgb_float_to_uint8(x: np.ndarray, check_data_loss: bool = True) -> np.ndarray:
    """RGB float to uint8"""
    assert x.dtype == np.float32
    assert len(x.shape) == 1 or (len(x.shape) == 2 and x.shape[1] == 1), f"Expected flattened data, got: {x.shape}"
    # [0 : 1] => [0 : 255^3 - 1]
    x_uint24 = np.int32(x * (256 * 256 * 256 - 1))
    # Shrink any additional bits outside of 24 bits
    x_uint24 = x_uint24 & (256 * 256 * 256 - 1)
    y = rgb_flat_uint24_to_uint8(x_uint24, check_correctness=False)

    if check_data_loss:
        assert np.allclose(x, rgb_float_to_uint8(y))
    return y

def rgb_uint8_to_flat_uint24(x: np.ndarray) -> np.ndarray:
    """RGB uint8 to flat uint24 (actually stored as uint32)"""
    assert x.shape[-1] == 3, f"Expcted 3 channels, got: {x.shape}"
    assert x.dtype == np.uint8

    # Convert the 3-channeled [0 : 255] data to float (no data loss)
    uint32_x = x.astype(np.uint32)
    # Serialize the 3-channeled [0 : 255] to [0 : 255^3 - 1]
    flattened_x = (uint32_x[..., 0] + uint32_x[..., 1] * 256 + uint32_x[..., 2] * 256 * 256)
    y = flattened_x.astype(np.uint32)
    return y

def rgb_flat_uint24_to_uint8(x: np.ndarray, check_correctness: bool = True) -> np.ndarray:
    """RGB flat uint24 (stored as uint32) to 3 channeled uint8 RGB"""
    if check_correctness:
        assert np.max(x) <= 256 * 256 * 256 - 1, "Data has values larger than an uint24"
    # Get the red, green, blue components and stack them back together as uint8
    R = x & 255
    G = (x >> 8) & 255
    B = (x >> 16) & 255
    y = np.array([R, G, B], dtype=np.uint8).transpose(1, 0)
    return y
