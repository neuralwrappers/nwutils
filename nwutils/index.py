"""Index utils"""
from typing import List, Union, Callable
import numpy as np

def smart_index_getter(data:Union[List, np.ndarray], indexes:List[int], f: Callable[[List, int], List] = None):
    """
    Smart index getter for a compount data structure with indexes (like lists of lists or custom arrays).
    Example: For index=[[1, 3], [15, 13]], first flatten the index: [1, 3, 15, 13], then apply the function to each
        item individually: [f(data, 1), f(data, 3), f(data, 15), f(data, 13)] and finally, reshape the result according
        to the original index shape: [[f(data, 1), f(data, 3)], [f(data, 15), f(data, 13)]]
    """
    def _simple_get(data, index):
        return data[index]

    if f is None:
        f = _simple_get
    if isinstance(indexes, (range, slice)):
        return smart_index_getter(data, np.arange(indexes.start, indexes.stop), f)[()]

    # Flatten the indexes [[1, 3], [15, 13]] => [1, 3, 15, 13]
    indexes = np.array(indexes, dtype=np.uint32)
    flattened_indexes = indexes.flatten()
    N = len(flattened_indexes)
    assert N > 0

    # Apply the transform function f to all the items of the flattened index
    result = []
    for i in range(N):
        item = f(data, flattened_indexes[i])
        result.append(item)

    # Reshape to original shape
    final_shape = (*indexes.shape, *result[0].shape)
    result = np.array(result).reshape(final_shape)
    return result
