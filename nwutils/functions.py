"""Function utilities"""
from typing import Callable, List, Sequence, Any
from pathlib import Path
import importlib
from .logger import logger

def try_fn(fn: Callable, n_tries: int, *args, **kwargs):
    """Try to call some specific function n times before bailing."""
    if n_tries is None:
        n_tries = 5

    for i in range(n_tries):
        try:
            return fn(*args, **kwargs)
        except Exception as e: #pylint: disable=broad-except
            logger.debug(f"Failed {i + 1}/{n_tries}. Function: {fn}. Error: {e}")
            continue
    assert False

def map_list(fns: List[Callable], seq: Sequence) -> Sequence:
    """Applies a list of map functions to a sequence. map(f2, map(f1, x)) => map_list([f1, f2], x)"""
    y = seq
    for fn in fns:
        y = map(fn, y)
    return y

def identity_fn(x, *args):
    """Identitfy function f(x) = x and f(a, b, c) = (a, b, c)"""
    if args:
        return x, *args
    return x

def module_from_file(module_name: str, file_path: Path) -> Any:
    """
    Returns a class or function by name from an arbitrary file given its path.
    Usage:
    - X_type = module_from_file("ClassName", "/path/to/module.py")
    - X = X_type(params)
    """
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return getattr(module, module_name)
