"""H5py utils"""
from typing import Any, Dict, Union
from pathlib import Path
import h5py
import numpy as np
from .index import smart_index_getter

def h5_get_smart_index_data(dataset: h5py._hl.group.Group, index: Any):
    """Wrapper on top of data[key] with support for ranges, slices, lists, tuples or arrays as smart indexes"""
    if isinstance(index, (range, slice)):
        return dataset[index.start : index.stop][()]
    if isinstance(index, (np.ndarray, list, tuple)):
        return smart_index_getter(dataset, index)
    assert False, f"Unknown type: '{type(index)}'"

def h5_export_dict_to_file(path: Union[Path, str], data: Dict, overwrite: bool = False) -> h5py._hl.dataset.Dataset:
    """Writes dict data to a h5 file and exports it to the desired path"""
    assert isinstance(data, dict)
    if overwrite is False:
        assert not Path(path).exists(), "File '{path}' already exists."
    h5_file = h5py.File(path, "w")
    h5_store_dict(h5_file, data)
    h5_file.flush()
    return h5_file

# pylint: disable=protected-access
def h5_store_dict(h5_file: Union[h5py._hl.group.Group, h5py._hl.dataset.Dataset], data: Dict):
    """Stores data in a provided h5 file or group. If data is nested, it will recursively create groups and store."""
    assert isinstance(data, dict)
    assert isinstance(h5_file, (h5py._hl.group.Group, h5py._hl.dataset.Dataset))
    for key in data.keys():
        # If key is int, we need to convert it to Str, so we can store it in h5 file.
        string_key = str(key) if isinstance(key, int) else key
        this_data = data[key]

        # Recursively create a group and repeat the process
        if isinstance(this_data, dict):
            h5_file.create_group(string_key)
            h5_store_dict(h5_file[string_key], this_data)
        # Regular data assignment
        else:
            h5_file[string_key] = this_data

# pylint: disable=protected-access
def h5_read_dict(data: Union[h5py._hl.files.File, h5py._hl.group.Group, h5py._hl.dataset.Dataset],
                 N: int = None) -> Any:
    """Reads data from a h5 file. If the file is a compound one with groups, it will return a nested dict."""
    if isinstance(data, (h5py._hl.files.File, h5py._hl.group.Group)):
        res = {}
        for key in data:
            res[key] = h5_read_dict(data[key], N)
        return res

    if isinstance(data, h5py._hl.dataset.Dataset):
        if N is None:
            return data[()]
        if isinstance(N, int):
            return data[0 : N]
        if isinstance(N, (list, np.ndarray)):
            return smart_index_getter(data, N)

    assert False, f"Unexpected type {type(data)}"
