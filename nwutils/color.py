"""Color utilities"""
from typing import Tuple

ColorType = Tuple[int, int, int]
Red = [255, 0, 0]
Green = [0, 255, 0]
Blue = [0, 0, 255]
Black = [0, 0, 0]
White = [255, 255, 255]
Gray = [127, 127, 127]
