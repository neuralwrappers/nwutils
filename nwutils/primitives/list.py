"""List utils"""
from typing import List, T

def flatten_list(x: List[List[T]]) -> List[T]:
    """Recursively flattens a list of lists to a 1D list"""
    assert isinstance(x, list), f"Got {type(x)}"
    if len(x) == 0:
        return []
    res = []
    for item in x:
        if isinstance(item, list):
            res.extend(flatten_list(item))
        else:
            res.append(item)
    return res
