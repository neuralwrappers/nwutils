"""Dictionary utils functions"""
from copy import deepcopy
from typing import Dict, Any, TypeVar, List
from ..data_structures import deep_check_equal

def dict_deep_get(nested_dict: Dict, key: Any) -> Any:
    """
    Parameters:
        nested_dict The nested dictionary key we want to look up in the nested dictionary
        key The nested key inside the dict
    Returns: The value of the nested dictionary according to the nested key
    """
    if isinstance(key, list):
        if len(key) == 1:
            return nested_dict[key[0]]
        return dict_deep_get(nested_dict[key[0]], key[1 :])

    if isinstance(key, tuple):
        if len(key) == 1:
            try:
                return nested_dict[key[0]]
            except KeyError:
                # We could have a tuple of length 1, too.
                return nested_dict[key]
        return dict_deep_get(nested_dict[key[0]], key[1 :])

    return nested_dict[key]

def dict_merge(dict1: Dict, dict2: Dict, inplace: bool = False) -> Dict:
    """
    Merges two disjoin dictionaries. If there is any key clash, an assert is thrown.
    Parameters:
        dict1 The first dictionary
        dict2 The second dictionary
        inplace If true, dict1 will be updated with dict2's keys, otherwise a new dictionary is created
    Returns: The merged dictionary
    """
    res = deepcopy(dict1) if inplace is False else dict1
    for k in dict2.keys():
        assert (not k in dict1) or (k in dict1 and not deep_check_equal(dict1[k], dict2[k])), \
            f"Key clash '{k}'.\n -Present: {dict1[k]}\n -New: {dict2[k]}"
        res[k] = dict2[k]
    return res

def dict_reodrerer(orig_dict: Dict[TypeVar("T"), TypeVar("T2")], indexer: List[TypeVar("T")]) \
                  -> Dict[TypeVar("T1"), TypeVar("T2")]:
    """Properly reorders a dictionary according to some key indexer and returns the newly ordered dict. As of
    python3.6 dictionaries are ordered, so we don't need to use OrderedDict() here.
    """
    return {k: orig_dict[k] for k in indexer}
