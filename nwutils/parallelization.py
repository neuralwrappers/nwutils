"""Parallelization utils"""
from torch.multiprocessing import Pool, set_start_method, cpu_count
from tqdm import tqdm


def pool_map_pbar(f, items, n_cores: int = 0, pbar: bool = True):
    """Pool map with progress bar"""
    try:
        set_start_method("spawn", force=True)
    except RuntimeError:
        pass

    n_cores = cpu_count() if n_cores is None else n_cores
    n_cores = min(cpu_count(), len(items), n_cores)
    if n_cores == 0:
        _range = tqdm(items) if pbar else items
        res = [f(x) for x in _range]
    else:
        res = []
        with Pool(n_cores) as pool:
            if pbar:
                pbar = tqdm(total=len(items))

            for one_res in pool.map(f, items):
                if pbar:
                    pbar.update()
                res.append(one_res)
    return res
