"""Bounding box related utilities"""
import numpy as np
import cv2

from .color import ColorType, Black

# TODO: smooth_bbox (without squaring, keeping median proportions)

# pylint: disable=unsupported-assignment-operation
def bbox_batch_smooth_and_square(bbox_batch: np.ndarray, smooth_delta: int = 10) -> np.ndarray:
    """Given a set of extremes (w_min, w_max, h_min, h_max), return a new set of smoothed keypoints that are also
    rectangle, based on the center of the median differences.
    Parameters:
        bbox The original unsmoothed and unsquared bounding box
        smooth_delta The number of frames on which to apply the smoothing. Defaults to 10.
            Current item will be smoothed w.r.t the [x - delta / 2, ..., x, ..., x + delta / 2]
            boxes (except at the end, using mode="valid").
    """
    assert len(bbox_batch.shape) == 2 and bbox_batch.shape[1] == 4, f"Wrong bbox shape: {bbox_batch.shape}"

    x1, x2, y1, y2 = bbox_batch.T

    # Based on all extremes, get the median frame and work with that
    x_center = (x1 + x2) // 2
    y_center = (y1 + y2) // 2

    # Get median of differences
    x_median = np.median(x2 - x1)
    y_median = np.median(y2 - y1)

    # Based on center +/- diffs, get the extremities of the bbox
    x1 = np.int32(x_center - x_median // 2)
    x2 = np.int32(x_center + x_median // 2)
    y1 = np.int32(y_center - y_median // 2)
    y2 = np.int32(y_center + y_median // 2)

    # Smooth the result further by doing a running mean with a M window (default 10)
    weight = np.ones(smooth_delta) / smooth_delta
    l, r = smooth_delta // 2, - smooth_delta // 2 + (smooth_delta % 2 == 0)
    x1[l : r] = np.convolve(x1, weight, mode="valid")
    x2[l : r] = np.convolve(x2, weight, mode="valid")
    y1[l : r] = np.convolve(y1, weight, mode="valid")
    y2[l : r] = np.convolve(y2, weight, mode="valid")

    # Fix the positions different than the median
    x_diff = x2 - x1
    x_diff = x_diff - np.median(x_diff)
    x_diff = np.int32(x_diff)
    y_diff = y2 - y1
    y_diff = y_diff - np.median(x_diff)
    y_diff = np.int32(y_diff)

    # Subtract the difference from the median to make it rectangle
    x2 -= x_diff
    y2 -= y_diff
    assert (x2 - x1).std() <= 1e-5
    assert (y2 - y1).std() <= 1e-5

    # Stack them back
    new_bbox_batch = np.stack([x1, x2, y1, y2], axis=1).astype(np.int32)
    return new_bbox_batch

def bbox_image_get_padding(image: np.ndarray, bbox: np.ndarray) -> np.ndarray:
    """Automatically get the padding of an image w.r.t the detected bounding box."""
    assert len(bbox.shape) == 1 and bbox.shape[0] == 4, f"Wrong bbox shape: {bbox.shape}"
    x1, x2, y1, y2 = bbox
    h, w = image.shape[0 : 2]
    padding = x1, w - x2, y1, h - y2
    assert padding[0] >= 0 and padding[1] >= 0 and padding[2] >= 0 and padding[3] >= 0
    return np.array(padding)

def bbox_make_square(bbox: np.ndarray, how: str="max") -> np.ndarray:
    """Given an unsquared bounding box, square it according to biggest or smallest line between widths and heights"""
    assert len(bbox.shape) == 1 and bbox.shape[0] == 4, f"Wrong bbox shape: {bbox.shape}"
    assert how in ("max", "min")
    x1, x2, y1, y2 = bbox

    # Force square by middle point
    x_half = (x2 + x1) // 2
    y_half = (y2 + y1) // 2
    line = max(y2 - y1, x2 - x1) if how == "max" else min(y2 - y1, x2 - x1)
    half_line = line // 2
    x1, x2, y1, y2 = x_half - half_line, y_half + half_line, y_half - half_line, y_half + half_line
    return x1, x2, y1, y2

def bbox_image_plot(image: np.ndarray, bbox: np.ndarray, thickness_percent: float = 1,
                    color: ColorType = None) -> np.ndarray:
    """Given an image and a bounding box, plot the bounding box according to some desired thickness"""
    assert image.dtype == np.uint8
    assert len(bbox.shape) == 1 and bbox.shape[0] == 4, f"Wrong bbox shape: {bbox.shape}"
    assert 0 <= thickness_percent <= 100
    if color is None:
        color = Black
    x1, x2, y1, y2 = bbox
    h, w = image.shape[0], image.shape[1]
    thickness = int(min(h, w) * thickness_percent / 100)
    assert thickness >= 1, f"Thickness percent f{thickness_percent} of {image.shape} leads to a value" \
                           f"too small: {thickness}"

    image = cv2.rectangle(image.copy(), (x1, y1), (x2, y2), color, thickness) # pylint: disable=no-member
    return np.array(image, dtype=np.uint8)

def bbox_pad(bbox: np.ndarray, padding: np.ndarray) -> np.ndarray:
    """Pad a bbox, according to a given padding"""
    assert len(bbox.shape) == 1 and bbox.shape[0] == 4, f"Wrong bbox shape: {bbox.shape}"
    assert len(padding.shape) == 2 and padding.shape[1] == 4, f"Wrong padding shape: {padding.shape}"

    x1, x2, y1, y2 = bbox
    x1 = x1 - padding[0]
    x2 = x2 + padding[1]
    y1 = y1 - padding[2]
    y2 = y2 + padding[3]
    return x1, x2, y1, y2

class TimeSmoothBbox:
    """Time smooth a time series of bounding boxes according to a running mean"""
    def __init__(self, t: int):
        self.rm = None
        self.t = t

    def __call__(self, bbox: np.ndarray) -> np.ndarray:
        assert bbox.dtype == np.int, bbox.dtype
        assert len(bbox.shape) == 1 and bbox.shape[0] == 4, f"Wrong bbox shape: {bbox.shape}"
        if self.rm is None:
            self.rm = np.array(bbox, dtype=bbox.dtype)
        self.rm = ((self.rm * (self.t - 1) + bbox).astype(np.float32) / self.t).astype(bbox.dtype)
        return self.rm
