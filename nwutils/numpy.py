"""Numpy utility functions"""
from typing import T
import numpy as np
import torch as tr

def np_get_info(data: np.ndarray) -> str:
    """Statistics about a given numerical numpy array"""
    f_str = f"Shape: {data.shape}. Min: {data.min()}. Max: {data.max()}. " \
            f"Std: {np.std(data)}. Mean: {np.mean(data)}. Data type: {data.dtype}."
    return f_str


def np_pad_zeros_to_highest_dim(data: np.ndarray) -> np.ndarray:
    """
    Pad the last dimension of a list (of lists) to the highest member of those outer lists.
    Example:
        a = [[[1, 2], [1, 2, 3]], [[1, 2, 3, 4], [1, 2, 3]], [[1, 2], [1]]]
        This list has a shape of 2 x 2 x variable length. Only the last dimension must be paddable.
        This resulting array can be converted to a shape of 2 x 2 x maximum last dimension
        We compute this by flattening the entire array first: [[1, 2], [1, 2, 3], [1, 2, 3, 4], [1, 2, 3], [1, 2], [1]]
        The lengths are: [2, 3, 4, 3, 2, 1]. Therefore, the maximum of the last dimension is 4.
        Resulting array will have the shape: 2 x 2 x 4, with zeros at the end of each dimension, besides the 3rd.
    """
    data = np.array(data)
    # If arrays match well, no need to do anything.
    if data.dtype != np.object:
        return data.astype(np.float32)

    data_flattened = data.flatten()
    N = len(data_flattened)
    lengths = np.zeros(N, dtype=np.int32)

    for i in range(N):
        assert isinstance(data_flattened[i], (list, np.ndarray))
        lengths[i] = len(data_flattened[i])

    max_length = lengths.max()
    # Add the max length to the last dimension of the result
    res = np.zeros((*data_flattened.shape, max_length), dtype=np.float32)
    for i in range(N):
        res[i, 0 : lengths[i]] = data_flattened[i]

    res = res.reshape((*data.shape, max_length))
    return res

# Results come in torch format, but callbacks require numpy, so convert the results back to numpy format
def np_get_data(data: T) -> T:
    """Given a data structure with data that can be converted to numpy arrays, return the same data structure, but
        with all the items inside it already converted, if appliable
    """
    if data is None:
        return None

    if isinstance(data, (int, float)):
        return np.array([data])

    if isinstance(data, list):
        return [np_get_data(x) for x in data]

    if isinstance(data, tuple):
        return tuple(np_get_data(x) for x in data)

    if isinstance(data, set):
        return set(np_get_data(x) for x in data)

    if isinstance(data, dict):
        return {k : np_get_data(data[k]) for k in data}

    if isinstance(data, tr.Tensor):
        return data.detach().to("cpu").numpy()

    if isinstance(data, np.ndarray):
        return data

    if callable(data):
        return data

    if isinstance(data, str):
        return data

    if hasattr(data, "to_numpy"):
        return data.to_numpy()

    assert False, f"Got type {type(data)}"
