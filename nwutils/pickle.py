"""Pickle utils"""
import pickle
from typing import Any
from .logger import logger

def is_picklable(item: Any) -> bool:
    """Checks if the given item is picklable"""
    try:
        _ = pickle.dumps(item)
        return True
    except Exception as e: #pylint: disable=broad-except
        logger.debug(f"Item is not pickable: {e}")
        return False
