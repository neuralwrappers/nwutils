"""Data structures utils"""
from typing import Dict, List, T, Any
import numpy as np
import torch as tr

# pylint: disable=too-many-branches
def deep_check_equal(item1: Any, item2: Any) -> bool:
    """
    Deep checks if two items of the same type are equal.
    Variants:
        - Dictionaries are checked value by value
        - Numpy arrays and torch tensors are compared using np/tr.allclose
        - Lists and tuples are checked item by item
        - Otherwise, we check via regular '==' operator
    """
    assert isinstance(item1, type(item2)), f"Types {type(item1)} and {type(item2)} differ."

    if isinstance(item1, dict):
        if item1.keys() != item2.keys():
            return False
        for key in item1:
            if not deep_check_equal(item1[key], item2[key]):
                return False
        return True

    if isinstance(item1, np.ndarray):
        if not len(item1) == len(item2):
            return False

        if np.issubdtype(item1.dtype, np.number):
            return np.allclose(item1, item2)

        for i, _ in enumerate(item1):
            if not deep_check_equal(item1[i], item2[i]):
                return False
        return True

    if isinstance(item1, tr.Tensor):
        return tr.allclose(item1, item2)

    if isinstance(item1, (list, tuple)):
        if not len(item1) == len(item2):
            return False

        for i, _ in enumerate(item1):
            if not deep_check_equal(item1[i], item2[i]):
                return False
        return True

    return item1 == item2


def topological_sort(dependency_graph: Dict[T, List[T]]) -> List[T]:
    """
    Given a graph as a dict {Node : [Dependencies]}, returns a list [Node] ordered with a correct topological
        sort order. Applies Kahn's algorithm: https://ocw.cs.pub.ro/courses/pa/laboratoare/laborator-07
    """
    L, S = [], []

    # First step is to create a regular graph of {Node : [Children]}
    graph = {k :[] for k in dependency_graph.keys()}
    in_nodes_graph = {}
    for key in dependency_graph:
        for parent in dependency_graph[key]:
            assert parent in graph, f"Node '{parent}' is not in given graph: {graph.keys()}"
            graph[parent].append(key)
        # Transform the depGraph into a list of number of in-nodes
        in_nodes_graph[key] = len(dependency_graph[key])
        # Add nodes with no dependencies and start BFS from them
        if in_nodes_graph[key] == 0:
            S.append(key)

    while len(S) > 0:
        u = S.pop()
        L.append(u)

        for v in graph[u]:
            in_nodes_graph[v] -= 1
            if in_nodes_graph[v] == 0:
                S.insert(0, v)

    for key in in_nodes_graph.keys():
        assert in_nodes_graph[key] == 0, "Graph has cycles. Cannot do topological sort."
    return L
