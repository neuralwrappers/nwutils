"""Timer class."""

from typing import Callable
from datetime import datetime, timedelta


class Timer:  # pragma: no cover
    """Basic timer to use as context manager."""
    def __init__(self, msg: str = "", print_fn: Callable = print):
        if not msg.endswith(" "):
            msg = f"{msg} "
        self.msg = msg
        self.print_fn = print_fn
        self._start = None

    def __enter__(self):
        self._start = datetime.now()

    def __exit__(self, *args, **kwargs):
        end = datetime.now()
        took: timedelta = end - self._start
        post = ""
        if took.microseconds > 10000:
            post = " !!!"
        self.print_fn(f"{self.msg}Took: {took}{post}")
