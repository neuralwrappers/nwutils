"""Running mean module for various data types"""
from abc import abstractmethod, ABC
from typing import Union, Sequence, Dict
from numbers import Number
from overrides import overrides
import numpy as np
import torch as tr
from ..logger import logger

class RunningMean(ABC):
    """Main running mean class"""
    def __init__(self, init_value: Union[Number, Sequence, Dict]):
        self.value = init_value
        self.count = 0

    @abstractmethod
    def update(self, value: Union[Number, Sequence, Dict], count: int = None):
        """Update method"""

    @abstractmethod
    def update_batch(self, value: Sequence):
        """Update batch method"""

    @abstractmethod
    def get(self):
        """Get running mean method"""

    def __repr__(self):
        return str(self.get())

    def __str__(self):
        return str(self.get())

class _RunningMeanNumber(RunningMean):
    """Running mean for a number"""
    @overrides
    def update(self, value: Union[Number, Sequence, Dict], count: int = None):
        if isinstance(value, (np.ndarray, tr.Tensor)):
            value = value.item()
        assert isinstance(value, Number), type(value)
        if count is None:
            count = 1
        self.value += value
        self.count += count

    @overrides
    def update_batch(self, value: Sequence):
        count = len(value)
        value = np.array(value).sum(axis=0)
        self.update(value, count)

    @overrides
    def get(self):
        if self.count == 0:
            return 0
        return self.value / self.count

class _RunningMeanSequence(RunningMean):
    """Running mean for a sequence (tuple, list, np.ndarray)"""
    @overrides
    def update(self, value: Union[Number, Sequence, Dict], count: int = None):
        value = np.array(value)
        if count is None:
            count = 1
        self.value += value
        self.count += count

    @overrides
    def update_batch(self, value: Sequence):
        value = np.array(value)
        assert len(value.shape) == len(self.value.shape) + 1
        self.update(value.sum(axis=0), value.shape[0])

    @overrides
    def get(self):
        if self.count == 0:
            return 0
        return self.value / self.count

class _RunningMeanDict(RunningMean):
    """Running mean for a dict"""
    @overrides
    def update(self, value: Union[Number, Sequence, Dict], count: int = None):
        if count is None:
            count = 1
        self.value = {k: self.value[k] + value[k] for k in self.value}
        self.count += count

    @overrides
    def update_batch(self, value: Sequence):
        assert False, "Only valid for Number and Sequence"

    @overrides
    def get(self):
        if self.count == 0:
            return 0
        return {k:self.value[k] / self.count for k in self.value}

def running_mean(init_value: Union[Number, Sequence, Dict]) -> RunningMean:
    """Builds a running mean object, based on the type of the initial value. If not set or unknown, will
        assume it is a number.
    """
    if isinstance(init_value, Number):
        return _RunningMeanNumber(init_value) # type: ignore

    if isinstance(init_value, (list, tuple, set, np.ndarray)):
        return _RunningMeanSequence(np.array(init_value))

    if isinstance(init_value, dict): # type: ignore
        return _RunningMeanDict(init_value) # type: ignore

    logger.info(f"Doing a running mean on unknown type {type(init_value)}")
    return _RunningMeanNumber(init_value)
