"""Data normalization utils"""
from numbers import Number
from typing import Union
import numpy as np

# pylint: disable=redefined-builtin
def min_max(x: np.ndarray, min: Union[Number, np.ndarray] = None, max: Union[Number, np.ndarray] = None) -> np.ndarray:
    """Data min-max normalization. If min or max are not provided, they are infered from the data"""
    x = x.astype(np.float32)
    if min is None:
        min = x.min()
    if max is None:
        max = x.max()
    return (x - min) / (max - min + np.spacing(1))

def min_max_percntile(x: np.ndarray, low: int = 0, high: int = 100) -> np.ndarray:
    """Min max normalization based on percentiles from the data. Data is then clipped to these low/high values."""
    min, max = np.percentile(x, [low, high])
    x = np.clip(x, min, max)
    return min_max(x)

def standardize(x: np.ndarray, mean: Union[Number, np.ndarray] = None,
                std: Union[Number, np.ndarray] = None) -> np.ndarray:
    """Data standardization. If mean or std are not provided, they are infered from the data"""
    x = x.astype(np.float32)
    if mean is None:
        mean = x.mean()
    if std is None:
        std = x.std()
    return (x - mean) / (std + np.spacing(1))
