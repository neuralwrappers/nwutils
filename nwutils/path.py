"""Path utilities"""
import os
from typing import List, Union, Tuple
from pathlib import Path
import numpy as np
from natsort import natsorted
from .logger import logger

def change_directory(path: Union[str, Path], expect_exist: bool = None):
    """Change the currently working directory based on the first arugment. Creates if not exists. If exepct_exist
    is False and the directory exists, it will throw an error
    """
    if isinstance(path, str):
        path = Path(path)
    assert isinstance(path, Path), f"Got: {type(path)}"
    assert expect_exist in (True, False, None)
    if expect_exist in (True, False):
        assert path.exists() == expect_exist, f"Exists: '{path}'"
    path.mkdir(exist_ok=True, parents=True)
    logger.info(f"Changing to working directory: '{path}'")
    os.chdir(path)

# pylint: disable=too-many-arguments
def get_files_from_dir(x: Path, pattern: str = "*", randomize: bool = False, n_files: int = None, seed: int = 42,
                       return_index: bool = False) -> Union[List[Path], Tuple[List[Path], List[int]]]:
    """Given a directory, get all the files according to a pattern. Somehow similar to ls `pattern` | sort `params`
    Parameters:
        path The directory path we're exploring
        pattern The pattern we want to use to retrieve files from the direcotry (ls `pattern`)
        randomize If true, the results' order will be randomized (ls `pattern` | shuf)
        n_files If set, it will limit the results to the first  (ls `pattern` | head -n `N`). Applied after randomize.
        seed The seed to be used if randomize is set to true. Defaults to 42.
    Return: The list of files according to the pattern. If return_index is set to True, return the indices of the
            used paths as well.
    """
    y = Path(x)
    assert y.exists(), f"Directory '{y}' does not exist!"
    y = y.glob(pattern)
    y = [str(x) for x in y]
    y = natsorted(y)
    y = [Path(x).absolute() for x in y]
    y = np.array(y)
    indices = np.arange(len(y))
    if randomize:
        np.random.seed(seed)
        indices = np.random.permutation(len(y))
        y = y[indices]
    if n_files is not None:
        indices = indices[0 : n_files]
        y = y[0 : n_files]
    return y if return_index is False else (y, indices)
